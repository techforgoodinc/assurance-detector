import cv2
import subprocess

capture = cv2.VideoCapture(0)
l_img = cv2.imread('show.png')

x_offset=100
y_offset=270

while True:
    ret, frame = capture.read()
    s_img = cv2.resize(frame, (415, 267))
    l_img[y_offset:y_offset+s_img.shape[0], x_offset:x_offset+s_img.shape[1]] = s_img
    display = cv2.resize(l_img, (1080, 720))
    cv2.namedWindow("alert", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("alert",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    cv2.imshow("alert",display)
    subprocess.call(["/usr/bin/osascript", "-e", 'tell app "Finder" to set frontmost of process "python" to true'])
    cv2.waitKey(0)
