from flask import Flask
from threading import Thread

app = Flask(__name__)

@app.route("/safety")
def safety(value):
    return value

def uiTask(value):
    app.run(value)

if __name__ == '__main__':
    output_q = Queue()
    t1 = Thread(target=uiTask, args=(output_q))
    t1.deamon = True
    t1.start()
